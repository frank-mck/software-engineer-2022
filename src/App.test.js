import { render, screen } from '@testing-library/react';
import App from './App';

test('renders a title', () => {
  render(<App />);
  const linkElement = screen.getByText('Employees');
  expect(linkElement).toBeInTheDocument();
});

test("renders both input fields", () => {
  render(<App />);
  const nameInput = screen.getByTestId('name-input');
  const emailInput = screen.getByTestId('email-input');
  expect(nameInput).toBeInTheDocument();
  expect(emailInput).toBeInTheDocument();
})

test("renders a table", () => {
  render(<App />);
  const table = screen.getByTestId('table');
  expect(table).toBeInTheDocument();
})
