import React, { useState } from 'react';
import './App.css'
import AddEmployee from './Components/AddEmployee/AddEmployee';
import Table from './Components/Table/Table';

const App = () => {
  const [employee, setEmployee] = useState([]);

  return (
    <div className='App'>
      <div className='container'>
        <Table employee={employee} setEmployee={setEmployee} />   
        <AddEmployee setEmployee={setEmployee} employee={employee} />   
      </div>      
    </div>
  );
}

export default App;
