import { fireEvent, render, screen } from '@testing-library/react';
import AddEmployee from './AddEmployee'

describe('<AddEmployee />', () => {
  test('renders a form and button', () => {
    render(<AddEmployee />);
    const form = screen.getByTestId('form');
    const btn = screen.getByTestId('btn');

    expect(form).toBeInTheDocument();
    expect(btn).toBeInTheDocument();
  });

  test("change the input value for name", () => {
    render(<AddEmployee />);
    const input = screen.getByTestId('name-input');
    fireEvent.change(input, { target: { value: 'Frank' }}); 
    expect(input).toHaveValue('Frank') 
  });

  test("change the input value for email", () => {
    render(<AddEmployee />);
    const input = screen.getByTestId('email-input');
    fireEvent.change(input, { target: { value: 'test@test.com' }});  
    expect(input).toHaveValue('test@test.com')
  })
})