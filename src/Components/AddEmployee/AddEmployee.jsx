import React, { useState } from 'react'
import './AddEmployee.css'

const AddEmployee = ({ setEmployee, employee }) => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');

  const addName = (event) => {
    event.preventDefault();
    
    setEmployee([...employee, { name: name, email: email}]);
    setName('');
    setEmail('');
  }

  return (
    <div>
      <form data-testid='form' className='form' onSubmit={addName}>        
        <input
          required
          className='form__input'
          data-testid='name-input'
          value={name}
          type='text'
          onChange={(e) => setName(e.target.value)}
          placeholder='Enter name...'
        />
        <button data-testid='btn' className='form__btn' type='submit'>Add</button>

        <input
          data-testid='email-input'
          className='form__input'
          type='email'
          placeholder ='Enter email...'
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />     
      </form>
    </div>
  )
}

export default AddEmployee;