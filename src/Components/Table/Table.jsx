import React from 'react';
import './Table.css'
import Employee from '../Employee/Employee';

const Table = ({ employee, setEmployee }) => {

  const employees = employee.sort((a, b) => a.name.localeCompare(b.name))

  return (
    <div className='table-container'>
      <div className='table__title'>
        <h2>Employees</h2>
      </div>

      <table data-testid='table' className='table'>
        <colgroup>
          <col></col>
          <col></col>
          <col></col>
        </colgroup>

        <thead>         
          <tr>
            <th className='table__header'>
              Names
            </th>
            <th className='table__header'>
              Email
            </th>
            <th className='table__header--delete'>
              Del
            </th>           
          </tr>   
        </thead>

        <tbody className='table__body'>         
          {employees.map((person, index) => (                
            <Employee 
              key={index}
              setEmployee={setEmployee} 
              name={person.name} 
              employee={employee}
              email={person.email} 
            />               
          ))}           
        </tbody>
      </table>     
    </div>
  )
};

export default Table;
