import { render, screen } from '@testing-library/react';
import Table from './Table.jsx'

describe('<Table />', () => {
  test('renders a list of employees', () => {
    const setEmployee = jest.fn();
    render(<Table employee={[{name: 'frank', email:''}]} setEmployee={setEmployee}/>);
    const table = screen.getByTestId('table');
    expect(table).toHaveTextContent('frank')
  })
})