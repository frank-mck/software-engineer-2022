import React from 'react'

const Employee = ({ name, employee, setEmployee, email }) => {

  const deleteName = () => {
    const filteredNames = employee.filter((person) => {
      return (name !== person.name) || (email !== person.email);
    });

    setEmployee([...filteredNames]);
  }

  return (
    <tr className='table__row'  >
      <td data-testid='employee' className='table__cell' >
        {name}
      </td>        
      <td className='table__cell'>
        {email}
      </td>        
      <td className='table__cell--delete' onClick={deleteName} >
        <img 
          className='table__cell--delete__img' 
          src ='https://cdn-icons.flaticon.com/png/512/4442/premium/4442016.png?token=exp=1645799915~hmac=583ca997c240ad6d7f5bba9c1f6fa6a0'
          alt='icon'
          data-testid='delete-img'
        />
      </td>
    </tr>
      
  )
}

export default Employee;
