import { render, screen } from '@testing-library/react';

import Employee from './Employee'

describe('<Employee />', () => {
  test('renders a delete img', () => {
    render(<Employee />);
    const img = screen.getByTestId('delete-img');
    expect(img).toBeInTheDocument();
  });

  test('renders an employees name', () => {
    render(<Employee name={'Frank'} />);
    const name = screen.getByTestId('employee');
    expect(name).toHaveTextContent('Frank')
  })
}) 